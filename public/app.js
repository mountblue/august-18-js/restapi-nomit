let listID;
$(document).ready(() => {
  fetch('http://localhost:3000/lists')
    .then(response => response.json())
    .then((lists) => {
      lists.forEach((list) => {
        $('.custom-select').append(
          `<option value='${list.id}'>${list.name}</option>`,
        );
      });
      $('.custom-select').change(function () {
        // console.log($(this).val())
        getItemsOfList($(this).val());
      });
    });

  function getItemsOfList(listId) {
    $('.list-group').empty();
    listID = listId;
    // console.log(listId)
    fetch(`http://localhost:3000/list/${listId}/items`)
      .then(response => response.json())
      .then((items) => {
        items.forEach((item) => {
          let checked;
          if (item.status === 'false') checked = '';
          else checked = 'checked';
          $('.list-group').append(
            `<li class="list-group-item"><input type="checkbox" value="${
              item.id
            }" data-listId="${listId}"${checked}><label>${item.content}</label>`,
          );
        });
      })
      .then(() => {
        $('input[type="checkbox"]').click(function () {
          const itemId = $(this).val();
          const listid = $(this).attr('data-listId');
          let state;
          if ($(this).is(':checked')) state = 'true';
          else state = 'false';
          fetch(
            `http://localhost:3000/list/${listid}/item/${itemId}/?status=${state}`,
            {
              method: 'PUT',
            },
          );
        });
      });
  }
  $('button').click(() => {
    const content = $('input[type="text"]').val();
    // console.log(content);
    fetch(
      `http://localhost:3000/list/${listID}/items/?status=false&content=${content}`,
      {
        method: 'POST',
      },
    );

    getItemsOfList(listID);
  });
});
