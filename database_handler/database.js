const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('Todolist', sqlite3.OPEN_READWRITE, (err) => {
  if (err) return console.error(err.message);
  console.log('connected to the Database');
});

function getLists() {
  const lists = [];
  return new Promise((resolve, reject) => {
    db.all('select * from List', [], (err, rows) => {
      if (err) reject(err);
      rows.forEach((row) => {
        lists.push(row);
      });
      resolve(lists);
    });
  });
}

function createList(name) {
  return new Promise((resolve, reject) => {
    db.run('insert into List(name) values(?)', [name], (err) => {
      if (err) reject(err);
      else {
        db.all('select * from List where name=?', [name], (err, row) => {
          if (err) reject(err);
          else resolve(row);
        });
      }
    });
  });
}

function getListById(id) {
  return new Promise((resolve, reject) => {
    db.all('select * from List where id=?', [id], (err, row) => {
      if (err) reject(err);
      resolve(row);
    });
  });
}

function deleteListById(id) {
  return new Promise((resolve, reject) => {
    db.all('delete from List where id=?', [id], (err, row) => {
      if (err) reject(err);
      resolve(row);
    });
  });
}

function getItemsOfList(id) {
  return new Promise((resolve, reject) => {
    db.all('select * from ListItems where list_id=?', [id], (err, row) => {
      if (err) reject(err);
      resolve(row);
    });
  });
}

function createItemOfList(content, listId, status) {
  // console.log(content,listId,status)
  return new Promise((resolve, reject) => {
    db.run(
      'insert into ListItems(content,list_id,status) values(?,?,?)',
      [content, listId, status],
      (err) => {
        if (err) {
          reject(err);
        }
        db.all(
          'select * from ListItems where list_id=(?) and content=(?)',
          [listId, content],
          (err, row) => {
            if (err) {
              reject(err);
            }
            resolve(row);
          },
        );
      },
    );
  });
}

function getItemOfListById(listId, itemId) {
  return new Promise((resolve, reject) => {
    db.all(
      'select * from ListItems where list_id=(?) and id=(?)',
      [listId, itemId],
      (err, row) => {
        if (err) {
          reject(err);
        }
        resolve(row);
      },
    );
  });
}

function updateItemOfList(listId, itemId, status) {
  return new Promise((resolve, reject) => {
    // console.log(listId, itemId, status);
    db.run(
      'update ListItems set status=(?) where id=(?) and list_id=(?)',
      [status, itemId, listId],
      (err) => {
        if (err) reject(err);
        db.all(
          'select * from ListItems where id=(?) and list_id=(?)',
          [itemId, listId],
          (err, row) => {
            if (err) reject(err);
            resolve(row);
          },
        );
      },
    );
  });
}

function deleteItemFromList(listId, itemId) {
  return new Promise((resolve, reject) => {
    db.run(
      'delete from ListItems where id=(?) and list_id=(?)',
      [itemId, listId],
      (err) => {
        if (err) reject(err);
        resolve('item is deleted');
      },
    );
  });
}

module.exports = {
  getLists,
  createList,
  getListById,
  getItemsOfList,
  deleteListById,
  getItemOfListById,
  createItemOfList,
  updateItemOfList,
  deleteItemFromList,
};
