const express = require('express');

const app = express();
const database = require('./database.js');

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE');
  next();
});
app.get('/lists', (req, res) => {
  database
    .getLists()
    .then((lists) => {
      res.send(lists);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});
app.post('/lists', (req, res) => {
  database
    .createList(req.query.name)
    .then((list) => {
      res.send(list);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});
app.get('/lists/:listID', (req, res) => {
  database
    .getListById(req.params.listID)
    .then((list) => {
      res.send(list);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});
app.delete('/lists/:listID', (req, res) => {
  database.deleteListById(req.params.listID).catch((err) => {
    res.status(404);
    res.send(err);
  });
});
app.get('/list/:listID/items', (req, res) => {
  database
    .getItemsOfList(req.params.listID)
    .then((items) => {
      res.send(items);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});
app.post('/list/:listID/items', (req, res) => {
  database
    .createItemOfList(req.query.content, req.params.listID, req.query.status)
    .then((item) => {
      res.send(item);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});

app.get('/list/:listID/item/:itemID', (req, res) => {
  database
    .getItemOfListById(req.params.listID, req.params.itemID)
    .then((item) => {
      res.send(item);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});
app.put('/list/:listID/item/:itemID', (req, res) => {
  database
    .updateItemOfList(req.params.listID, req.params.itemID, req.query.status)
    .then((item) => {
      res.send(item);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});

app.delete('/list/:listID/item/:itemID', (req, res) => {
  database
    .deleteItemFromList(req.params.listID, req.params.itemID)
    .then((item) => {
      res.send(item);
    })
    .catch((err) => {
      res.status(404);
      res.send(err);
    });
});
app.listen(3000, () => {
  console.log('Listening to 3000');
});
